<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/quetal', function () {
    return "<a href='".route("nosotros")."'>nosotros</a>";
});

Route::get('/nosotros_perra', function () {
    return "<h1>Toda la información sobre nosotros </h1>";
})->name("nosotros");


Route::get('home/{nombre?}/{apellido?}', function ($nombre = 'Pepe', $apellido = 'grillo') {

    $posts = ["post1", "post2", "post3", "post4", "post5"];
    $posts2 = [];

    //return view('home')->with('nombre', $nombre)->with('apellido', $apellido);
    return view('home', ['nombre' => $nombre, 'apellido' => $apellido, 'posts' => $posts, 'posts2' => $posts2]);
})->name("home");