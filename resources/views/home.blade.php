<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Mi primera vista</title>
</head>
<body>
    <h1>Mundo laravel -  {{"Hola mundo $nombre $apellido "}}</h1>
    <ul>
        @isset($posts2) {{-- Entra si esta definida y no esta null --}}
            isset
        @endisset
        @empty($posts1) {{-- Entra si la variable esta vacia --}}
            empty
        @endempty

        @forelse ($posts as $post)

            <?php //dd($loop) ?>{{-- reemplaza a  var_dump ya que este trae mucha información --}}

            <li>
                @if ($loop->first) {{-- Preguntamos si esta en al primera posicion del array --}}
                    Primero: 
                @elseif ($loop->last) {{-- Preguntamos si esta en al ultima posicion del array --}}
                    Ultimo: 
                @else
                    Medio: 
                @endif 

                {{ $post }}
            </li>
            @empty {{-- Si esta vacio imprimimos vacio --}}
                <li>vacio</li>
        @endforelse
{{-- Cambio de prueba  --}}
    </ul>
</body>
</html>